# GPL Arcade Volleyball
Originally developed at https://sourceforge.net/projects/gav/ by [alley](http://sourceforge.net/users/alley), [asforza4](http://sourceforge.net/users/asforza4), [nardo](http://sourceforge.net/users/nardo) and [zavatt](http://sourceforge.net/users/zavatt) and published under the GPL-2.0 license.
